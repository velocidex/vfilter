module www.velocidex.com/golang/vfilter

require (
	github.com/Velocidex/ordereddict v0.0.0-20200723153557-9460a6764ab8
	github.com/alecthomas/participle v0.4.4
	github.com/alecthomas/repr v0.0.0-20181024024818-d37bc2a10ba1
	github.com/go-test/deep v1.0.5
	github.com/pkg/errors v0.8.1
	github.com/sebdah/goldie/v2 v2.5.3
	github.com/stretchr/testify v1.4.0
	golang.org/x/text v0.3.0
)

// replace github.com/Velocidex/ordereddict => /home/mic/projects/ordereddict

go 1.13
